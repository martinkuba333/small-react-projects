import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Links } from './Links/Links';

export default function App(): React.ReactNode {
  return (
    <Router>
      <Links />
    </Router>
  );
}
