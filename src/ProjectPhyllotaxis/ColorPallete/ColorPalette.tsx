export function colorPallete(tick: number): number[][] {
  return [
    [
      Math.floor(Math.random() * 256),
      Math.floor(Math.random() * 256),
      Math.floor(Math.random() * 256),
    ], // Case 1
    [tick % 256, 0, tick % 256], // Case 2
    [tick % 256, 0, 0], // Case 3
    [0, tick % 256, 0], // Case 4
    [0, 0, tick % 256], // Case 5
    [0, tick % 256, tick % 256], // Case 6
    [tick % 256, tick % 256, tick % 256], // Case 7
  ];
}
