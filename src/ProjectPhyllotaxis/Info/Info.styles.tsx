import styled from 'styled-components';

const textStyle = {
  textAlign: 'center',
  left: 0,
  right: 0,
  position: 'absolute',
  zIndex: 100,
};

export const Title = styled.h3`
  ${textStyle};
`;

export const Text = styled.p`
  ${textStyle};
  top: 40px;
`;
