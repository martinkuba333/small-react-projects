import React, { useEffect, useRef } from 'react';
import { Title, Text } from './Info.styles';

export const Info: React.FC = () => {
  const titleRef = useRef<HTMLHeadingElement>(null);
  const textRef = useRef<HTMLParagraphElement>(null);

  useEffect(() => {
    setTimeout(() => {
      if (titleRef.current) {
        titleRef.current.style.display = 'none';
      }
      if (textRef.current) {
        textRef.current.style.display = 'none';
      }
    }, 15000);
  }, []);

  return (
    <>
      <Title ref={titleRef}>Phyllotaxis</Title>
      <Text ref={textRef}>Mathematical patterns from nature with configurations</Text>
    </>
  );
};
