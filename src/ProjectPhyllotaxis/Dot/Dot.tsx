import React from 'react';
import { DotStyles } from './Dot.styles';

interface DotInterface {
  top: number;
  left: number;
  bgColor: string;
  index: number;
  size: number;
  borderColor: string;
}

export const Dot: React.FC<DotInterface> = (props: DotInterface) => {
  return (
    <>
      <DotStyles
        style={{
          borderColor: props.borderColor,
          width: props.size + 'px',
          height: props.size + 'px',
          top: `${props.top}px`,
          left: `${props.left}px`,
          background: `${props.index % 1 === 0 ? props.bgColor : 'transparent'}`,
        }}
      />
    </>
  );
};
