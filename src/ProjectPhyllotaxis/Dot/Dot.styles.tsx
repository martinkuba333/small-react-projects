import styled from 'styled-components';

export const DotStyles = styled.div`
  border-radius: 50%;
  position: absolute;
  border-width: 1px;
  border-style: solid;
`;
