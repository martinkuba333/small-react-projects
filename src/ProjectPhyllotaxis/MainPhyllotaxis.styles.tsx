import styled from 'styled-components';

export const Center = styled.div`
  width: 0px;
  height: 0px;
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: -100px;
  bottom: 0px;
`;
