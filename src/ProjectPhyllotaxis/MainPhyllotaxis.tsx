import React, { useEffect, useReducer } from 'react';
import { Center } from './MainPhyllotaxis.styles';
import { Dot } from './Dot/Dot';
import { CONST } from './Utils/Const';
import { Configuration } from './Configuration/Configuration';
import { isPrime } from './Utils/Helper';
import { colorPallete } from './ColorPallete/ColorPalette';
import { Info } from './Info/Info';

export interface State {
  tick: number;
  dots: JSX.Element[];
  angle: number;
  dotsize: number;
  rColor: number;
  gColor: number;
  bColor: number;
  scaling: number;
  borderColor: string;
  color: number;
  pattern: number;
}

const initialState: State = {
  tick: 0,
  dots: [],
  angle: CONST.ANGLE,
  dotsize: CONST.DOT_SIZE,
  rColor: 0,
  gColor: 0,
  bColor: 0,
  scaling: CONST.SCALING,
  borderColor: CONST.BORDER_COLOR,
  color: 1,
  pattern: 1,
};

type Action = Partial<State>;

function reducer(state: State, action: Action): State {
  return { ...state, ...action };
}

export const MainPhyllotaxis: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    const timeout = setTimeout(() => {
      createNewDot();
      dispatch({ tick: state.tick + 1 });
    }, CONST.SPEED);
    return () => clearTimeout(timeout);
  }, [state.tick]);

  const createNewDot = () => {
    const isPrimeNumber = isPrime(state.tick); //for various testing
    const rgb = getRandomColorSet();
    const r = state.scaling * Math.sqrt(state.tick);
    const angle = getRandomPattern();
    const tick = state.tick;
    const top = r * Math.cos(tick * angle) + CONST.DOT_SIZE / 2;
    const left = r * Math.sin(tick * angle) + CONST.DOT_SIZE / 2;

    const newDot = (
      <Dot
        key={state.tick}
        top={top}
        left={left}
        bgColor={rgb}
        index={state.tick}
        size={state.dotsize}
        borderColor={state.borderColor}
      />
    );
    dispatch({ dots: [...state.dots, newDot] });
  };

  const patterns: number[] = [
    state.angle,
    36,
    45,
    72,
    137.5 + state.tick / 2,
    40,
    222.5,
    90,
    0.1,
    210,
    144,
    state.tick % state.angle,
    137.5,
    273.3,
    190.6,
  ];

  const getRandomPattern = (): number => {
    let angle = patterns[state.pattern - 1];
    if (state.pattern === 12 || state.pattern === 1) {
      return angle;
    }
    updateConfiguration({ angle });
    return angle;
  };

  const getRandomColorSet = (): string => {
    const rColor = state.rColor;
    const gColor = state.gColor;
    const bColor = state.bColor;
    let rgb = '';

    const rgbValues = colorPallete(state.tick);

    switch (state.color) {
      case 1:
        rgb = `rgb(${rgbValues[0][0]},${rgbValues[0][1]},${rgbValues[0][2]})`;
        break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
        rgb = `rgb(${rgbValues[state.color - 1][0]},${rgbValues[state.color - 1][1]},${rgbValues[state.color - 1][2]})`;
        break;
      default:
        rgb = `rgb(${rColor},${gColor},${bColor})`;
        break;
    }
    return rgb;
  };

  const updateConfiguration = (config: Partial<State>) => {
    dispatch(config);
  };

  return (
    <>
      <Info />
      <Center>{state.dots}</Center>
      <Configuration patterns={patterns} state={state} updateConfiguration={updateConfiguration} />
    </>
  );
};
