import React, { useState } from 'react';
import {
  Label,
  ConfigContainer,
  Input,
  InputContainer,
  Button,
  SwitchBgColorButton,
} from './Configuration.styles';
import { CONFIG_ENUM, CONST } from '../Utils/Const';
import { State } from '../MainPhyllotaxis';
import { colorPallete } from '../ColorPallete/ColorPalette';

interface ConfigurationInterface {
  patterns: number[];
  state: State;
  updateConfiguration: (config: Partial<State>) => void;
}

enum BodyBgColors {
  White = 'white',
  Black = 'black',
}

export const Configuration: React.FC<ConfigurationInterface> = (props: ConfigurationInterface) => {
  const [bodyBgColor, setBodyBgColor] = useState<BodyBgColors>(BodyBgColors.White);

  const inputs = [
    {
      label: CONFIG_ENUM.SCALING,
      type: 'Range',
      min: CONST.MIN_SCALE,
      max: CONST.MAX_SCALE,
      step: 1,
      value: props.state.scaling,
      width: 100,
    },
    {
      label: CONFIG_ENUM.DOTSIZE,
      type: 'Range',
      min: CONST.MIN_SIZE,
      max: CONST.MAX_SIZE,
      step: 1,
      value: props.state.dotsize,
      width: 100,
    },
    {
      label: CONFIG_ENUM.ANGLE,
      type: 'Range',
      min: CONST.MIN_ANGLE,
      max: CONST.MAX_ANGLE,
      step: 0.1,
      value: props.state.angle,
      width: 300,
    },
  ];

  const handleChange = (value: string, label: string): void => {
    const parsedValue = parseFloat(value);
    if (isNaN(parsedValue)) {
      return;
    }

    if (
      label === CONFIG_ENUM.SCALING &&
      parsedValue >= CONST.MIN_SCALE &&
      parsedValue <= CONST.MAX_SCALE
    ) {
      props.updateConfiguration({ [label]: parsedValue });
    }
    if (
      label === CONFIG_ENUM.DOTSIZE &&
      parsedValue >= CONST.MIN_SIZE &&
      parsedValue <= CONST.MAX_SIZE
    ) {
      props.updateConfiguration({ [label]: parsedValue });
    }
    if (
      label === CONFIG_ENUM.ANGLE &&
      parsedValue >= CONST.MIN_ANGLE &&
      parsedValue <= CONST.MAX_ANGLE
    ) {
      props.updateConfiguration({ [label]: parsedValue, pattern: 1 });
    }
  };

  const createInputs = (): JSX.Element => {
    const createdInputs = [];
    for (let i = 0; i < inputs.length; i++) {
      createdInputs.push(
        <InputContainer key={inputs[i].label}>
          <Label>
            {inputs[i].label}: {inputs[i].value}
          </Label>
          <br />
          <Input
            type={inputs[i].type}
            id={inputs[i].label + 'Input'}
            name={inputs[i].label + 'Input'}
            min={inputs[i].min}
            max={inputs[i].max}
            step={inputs[i].step}
            value={inputs[i].value}
            style={{ width: inputs[i].width + 'px' }}
            onChange={(e: any) => handleChange(e.target.value, inputs[i].label)}
          />
        </InputContainer>,
      );
    }
    return <>{createdInputs}</>;
  };

  const cleanScreen = () => {
    props.updateConfiguration({ dots: [], tick: 0 });
  };

  const changeColorPallete = () => {
    const paletteLength = colorPallete(0).length;
    props.updateConfiguration({
      color: props.state.color > paletteLength ? 1 : props.state.color + 1,
    });
  };

  const changePattern = () => {
    props.updateConfiguration({
      pattern: props.state.pattern >= props.patterns.length ? 2 : props.state.pattern + 1,
      dots: [],
      tick: 0,
    });
  };

  const changeBodyBgColor = () => {
    const body = document.body;
    const newBgColor = bodyBgColor === BodyBgColors.Black ? BodyBgColors.White : BodyBgColors.Black;
    body.style.backgroundColor = newBgColor;
    setBodyBgColor(newBgColor);
    props.updateConfiguration({
      borderColor: newBgColor,
    });
  };

  const createButtons = (): JSX.Element => {
    return (
      <>
        <Button onClick={cleanScreen}>Clean Screen</Button>
        <Button onClick={changeColorPallete}>Color palette: {props.state.color}</Button>
        <Button onClick={changePattern}>Predefined pattern: {props.state.pattern}</Button>
        <SwitchBgColorButton onClick={changeBodyBgColor}>Change Background</SwitchBgColorButton>
      </>
    );
  };

  return (
    <>
      <ConfigContainer>
        {createInputs()}
        <br />
        <br />
        {createButtons()}
      </ConfigContainer>
    </>
  );
};
