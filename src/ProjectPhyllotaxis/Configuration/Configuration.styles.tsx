import styled from 'styled-components';

export const ConfigContainer = styled.div`
  background: rgba(255, 255, 255, 0.9);
  border-top: 2px solid black;
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
`;

const buttonCss = {
  display: 'inline-block',
  background: 'gold',
  padding: '10px',
  borderRadius: '5px',
  position: 'relative',
  top: '-9px',
  userSelect: 'none',
};

export const Button = styled.button`
  ${buttonCss}

  &:hover {
    cursor: pointer;
  }
`;

export const SwitchBgColorButton = styled.button`
  ${buttonCss}

  &:hover {
    cursor: pointer;
  }
`;

export const InputContainer = styled.div`
  display: inline-block;
  background: #efefef;
  width: fit-content;
  margin-right: 15px;
  padding: 5px;
`;

export const Label = styled.label`
  user-select: none;
  text-transform: uppercase;
  text-align: center;
`;

export const Input = styled.input`
  width: 100%;
  &:hover {
    cursor: pointer;
  }
`;
