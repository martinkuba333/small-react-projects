export const CONST = {
  SPEED: 1,
  ANGLE: 137.5, //137.5 is the best
  SCALING: 4,
  DOT_SIZE: 5,
  BORDER_COLOR: 'white',
  MIN_SCALE: 1,
  MAX_SCALE: 10,
  MIN_SIZE: 1,
  MAX_SIZE: 10,
  MIN_ANGLE: 0.1,
  MAX_ANGLE: 360,
};

export const CONFIG_ENUM = {
  SCALING: 'scaling',
  DOTSIZE: 'dotsize',
  ANGLE: 'angle',
};

//todo - ideas
//change behaviour in the middle
//show only prime/odd/even numbers
//responsive - some weird mobile move
