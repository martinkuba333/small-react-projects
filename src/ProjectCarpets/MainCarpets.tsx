import React, { useEffect, useState } from 'react';
import { Board } from './Board/Board';
import { CONST } from './Utils/Const';

interface MainCarpetProps {}

export const MainCarpets: React.FC<MainCarpetProps> = () => {
  const [tick, setTick] = useState<number>(6);

  useEffect(() => {
    const squareInterval = setTimeout(() => {
      let newTick = tick;
      if (newTick === 0) {
        newTick = 1;
      } else if (newTick === 1) {
        newTick = 4;
      } else if (newTick === 4) {
        newTick = 5;
      } else if (newTick === 5) {
        newTick = 11;
      } else if (newTick === 11) {
        newTick = 6;
      } else if (newTick === 6) {
        newTick = 0;
      }

      setTick(newTick);
    }, CONST.INTERVAL_SPEED);
    return () => clearTimeout(squareInterval);
  }, [tick]);

  return (
    <>
      <Board tick={tick} />
    </>
  );
};
