import styled from 'styled-components';
import { CONST } from '../Utils/Const';

export const SquareContainer = styled.div`
  width: ${CONST.SQUARE_WIDTH * CONST.SQUARE_LENGTH}px;
  position: relative;
`;

export const Square = styled.div`
  display: block;
  width: ${CONST.SQUARE_WIDTH}px;
  height: ${CONST.SQUARE_WIDTH}px;
  position: relative;
  float: left;
  transition: all 0.7s ease;
`;
