import React, { useEffect, useState } from 'react';
import { Square, SquareContainer } from './Carpet.styles';
import { COLOR_NUMBERS, CONST, NUMBERS } from '../Utils/Const';

export type HexColor = `${string & { length: 7 }}`;
type TopValue = 0 | typeof CONST.SQUARE_MOVEMENT;

interface CarpetInterface {
  tick: number;
  index: number;
  colorSet: HexColor[];
}

export const Carpet: React.FC<CarpetInterface> = (props: CarpetInterface) => {
  const [squarePosition, setSquarePosition] = useState<TopValue[]>(
    Array.from({ length: CONST.SQUARE_LENGTH * CONST.SQUARE_LENGTH }, () =>
      props.index < (CONST.CARPETS_LINE * CONST.CARPETS_LINE) / 2 ? 0 : CONST.SQUARE_MOVEMENT,
    ),
  );
  const [moveDown, setMoveDown] = useState<boolean>(
    props.index < (CONST.CARPETS_LINE * CONST.CARPETS_LINE) / 2,
  );

  let counter = 0;

  useEffect(() => {
    if (
      (props.tick === 0 &&
        (props.tick === props.index ||
          props.tick + 7 === props.index ||
          props.tick + 14 === props.index ||
          props.tick + 21 === props.index ||
          props.tick + 28 === props.index ||
          props.tick + 35 === props.index)) ||
      (props.tick === 1 &&
        (props.tick === props.index ||
          props.tick + 1 === props.index ||
          props.tick + 7 === props.index ||
          props.tick + 26 === props.index ||
          props.tick + 32 === props.index ||
          props.tick + 33 === props.index)) ||
      (props.tick === 4 &&
        (props.tick === props.index ||
          props.tick - 1 === props.index ||
          props.tick + 5 === props.index ||
          props.tick + 22 === props.index ||
          props.tick + 27 === props.index ||
          props.tick + 28 === props.index)) ||
      (props.tick === 5 &&
        (props.tick === props.index ||
          props.tick + 5 === props.index ||
          props.tick + 10 === props.index ||
          props.tick + 15 === props.index ||
          props.tick + 20 === props.index ||
          props.tick + 25 === props.index)) ||
      (props.tick === 11 &&
        (props.tick === props.index ||
          props.tick + 5 === props.index ||
          props.tick + 6 === props.index ||
          props.tick + 7 === props.index ||
          props.tick + 8 === props.index ||
          props.tick + 13 === props.index)) ||
      (props.tick === 6 &&
        (props.tick === props.index ||
          props.tick + 6 === props.index ||
          props.tick + 7 === props.index ||
          props.tick + 16 === props.index ||
          props.tick + 17 === props.index ||
          props.tick + 23 === props.index))
    ) {
      const interval = setInterval(() => {
        setSquarePosition((prevTops: TopValue[]) => {
          let updatedTops = [...prevTops];

          updatedTops = calculateNewTops(updatedTops);

          counter++;
          return updatedTops;
        });

        if (counter > (CONST.CARPETS_LINE * CONST.CARPETS_LINE) / 2) {
          clearInterval(interval);
        }
      }, CONST.MOVEMENT_DELAY);

      return () => clearInterval(interval);
    }
    return;
  }, [props.tick]);

  const calculateNewTops = (updatedTops: TopValue[]): TopValue[] => {
    const index = Math.floor(counter / 2);
    const nums = NUMBERS[index];

    if (nums) {
      for (let i = 0; i < nums.length; i++) {
        updatedTops[nums[i]] = moveDown ? CONST.SQUARE_MOVEMENT : 0;
      }
      const newMoveDown = !moveDown;
      setMoveDown(newMoveDown);
    }

    return updatedTops;
  };

  const createBgColor = (number: number): HexColor => {
    const colorSet: HexColor[] = props.colorSet;

    for (let i = 0; i < COLOR_NUMBERS.length; i++) {
      if (COLOR_NUMBERS[i].includes(number)) {
        return colorSet[i];
      }
    }

    return '#000000' as HexColor;
  };

  const createSquares = (): JSX.Element[] => {
    const result: JSX.Element[] = [];
    for (let i = 0; i < squarePosition.length; i++) {
      const bgColor = createBgColor(i);
      result.push(
        <Square
          key={i}
          style={{
            top: squarePosition[i] + 'px',
            left: squarePosition[i] + 'px',
            background: bgColor,
          }}
        />,
      );
    }
    return result;
  };

  return (
    <>
      <SquareContainer>{createSquares()}</SquareContainer>
    </>
  );
};
