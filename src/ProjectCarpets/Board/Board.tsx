import React, { useState } from 'react';
import { Carpet, HexColor } from '../Carpet/Carpet';
import { Area, CarpetContainer, RandomColorButton } from './Board.styles';
import { COLOR_INDEX, COLOR_SET, CONST } from '../Utils/Const';

interface BoardInterface {
  tick: number;
}

export const Board: React.FC<BoardInterface> = (props: BoardInterface) => {
  const [colorSet, setColorSet] = useState<string[]>(COLOR_SET[COLOR_INDEX.BLUE]);

  const createCarpets = (): JSX.Element[] => {
    const carpets: JSX.Element[] = [];
    for (let i = 0; i < CONST.CARPETS_LINE * CONST.CARPETS_LINE; i++) {
      carpets.push(
        <CarpetContainer key={i}>
          <Carpet key={i} tick={props.tick} index={i} colorSet={colorSet as HexColor[]} />
        </CarpetContainer>,
      );
    }
    return carpets;
  };

  const processColorClick = () => {
    let randomIndex;
    do {
      randomIndex = Math.floor(Math.random() * Object.keys(COLOR_INDEX).length);
    } while (COLOR_SET[randomIndex] === colorSet);

    setColorSet(COLOR_SET[randomIndex]);
  };

  return (
    <>
      <Area>{createCarpets()}</Area>
      <RandomColorButton onClick={processColorClick}>Random Change Color</RandomColorButton>
    </>
  );
};
