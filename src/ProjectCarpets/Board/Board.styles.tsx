import styled from 'styled-components';

export const Area = styled.div`
  width: 660px;
  margin: auto;
  transform: rotateX(45deg) rotateZ(45deg);
`;

export const CarpetContainer = styled.div`
  display: inline-block;
  position: relative;
  width: 110px;
  height: 106px;
`;

export const RandomColorButton = styled.div`
  border: 2px solid black;
  text-align: center;
  width: fit-content;
  margin: 5px auto;
  position: absolute;
  left: 0;
  right: 0;
  top: 750px;
  padding: 5px 15px;
  border-radius: 5px;
  color: black;
  display: inline-block;
  user-select: none;

  &:hover {
    color: white;
    background: black;
    user-select: none;
    cursor: pointer;
  }
`;
