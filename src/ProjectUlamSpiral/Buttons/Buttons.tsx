import React, { useState } from 'react';
import { ButtonsContainer, Label, SubmitButton } from './Buttons.styles';

interface ButtonsInterface {
  startingValue: number;
  setStartingValue: React.Dispatch<React.SetStateAction<number>>;
}

export const Buttons: React.FC<ButtonsInterface> = (props: ButtonsInterface): JSX.Element => {
  const [inputValue, setInputValue] = useState<number | ''>(props.startingValue);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    if (!isNaN(parseInt(value)) && parseInt(value) >= 0) {
      setInputValue(parseInt(value));
    }
    if (value === '') {
      setInputValue(value);
    }
  };

  const handleSubmit = () => {
    if (inputValue !== '') {
      props.setStartingValue(inputValue);
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      handleSubmit();
    }
  };

  return (
    <>
      <ButtonsContainer>
        <Label>Choose Starting Number:</Label>
        <br />
        <input
          type="text"
          id="startingNumber"
          name="startingNumber"
          value={inputValue}
          onChange={handleInputChange}
          onKeyDown={handleKeyDown}
        />
        <SubmitButton onClick={handleSubmit}>Submit</SubmitButton>
      </ButtonsContainer>
    </>
  );
};
