import styled from 'styled-components';

export const ButtonsContainer = styled.div`
  width: fit-content;
  position: absolute;
  bottom: 25px;
  margin: auto;
  left: 0;
  right: 0;
  text-align: center;
`;

export const Label = styled.label`
  line-height: 2;
  font-weight: bold;
`;

export const SubmitButton = styled.button`
  margin-top: 5px;
  background: white;
  border: 2px solid black;
  border-radius: 5px;
  transition: all 0.1s linear;

  &:hover {
    cursor: pointer;
    background: black;
    color: white;
  }
`;
