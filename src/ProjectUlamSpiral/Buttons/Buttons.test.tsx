import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { Buttons } from './Buttons';

describe('Buttons component', () => {
  const mockSetStartingValue = jest.fn();
  const { getByRole } = render(
    <Buttons startingValue={1} setStartingValue={mockSetStartingValue} />,
  );
  const input = getByRole('textbox');
  const submitBtn = getByRole('button', { name: 'Submit' });

  it('test click on submit button with valid input', () => {
    fireEvent.change(input, { target: { value: 10 } });
    fireEvent.click(submitBtn);
    expect(mockSetStartingValue).toBeCalled();
    expect(mockSetStartingValue).toHaveBeenCalledWith(10);

    fireEvent.change(input, { target: { value: 0 } });
    fireEvent.click(submitBtn);
    expect(mockSetStartingValue).toBeCalled();
    expect(mockSetStartingValue).toHaveBeenCalledWith(0);
  });

  it('test invalid input', () => {
    fireEvent.change(input, { target: { value: 'ahoj' } });
    fireEvent.click(submitBtn);
    expect(mockSetStartingValue).not.toBeCalled();

    fireEvent.change(input, { target: { value: '-7' } });
    fireEvent.click(submitBtn);
    expect(mockSetStartingValue).not.toBeCalled();
  });
});
