import React, { useEffect, useState } from 'react';
import { Center } from './Board.styles';
import { Point } from '../Point/Point';
import { CONST } from '../Utils/Constants';

interface BoardInterface {
  startingNum: number;
}

let top = 0;
let left = 0;
let turns = 0;
let spiralStep = 1;

export const Board: React.FC<BoardInterface> = (props: BoardInterface): JSX.Element => {
  const [tick, setTick] = useState<number>(-1);
  const [points, setPoints] = useState<JSX.Element[]>([
    <Point index={props.startingNum} key={props.startingNum} top={top} left={left} />,
  ]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setTick((prevState: number) => {
        return prevState + 1;
      });
    }, CONST.GENERATION_SPEED);
    return () => clearTimeout(timeout);
  }, [tick]);

  useEffect(() => {
    if (tick > 0) {
      createNewPoint(tick);
    }
  }, [tick]);

  useEffect(() => {
    resetSpiral();
  }, [props.startingNum]);

  const resetSpiral = () => {
    top = 0;
    left = 0;
    turns = 0;
    spiralStep = 1;
    setPoints([<Point index={props.startingNum} key={props.startingNum} top={top} left={left} />]);
    setTick(-1);
  };

  const createNewPoint = (index: number) => {
    const newPoints = [...points];
    calculatePositions(index);

    newPoints.push(
      <Point
        index={index + props.startingNum}
        key={index + props.startingNum}
        top={top}
        left={left}
      />,
    );
    setPoints(newPoints);
  };

  const calculatePositions = (index: number) => {
    if (turns === 0) {
      left += CONST.SPACE_SIZE;
      if (index % spiralStep === 0) {
        turns++;
      }
    } else if (turns === 1) {
      top -= CONST.SPACE_SIZE;
      if (index % spiralStep === 0) {
        turns++;
        spiralStep++;
      }
    } else if (turns === 2) {
      left -= CONST.SPACE_SIZE;
      if (index % spiralStep === 0) {
        turns++;
      }
    } else if (turns === 3) {
      top += CONST.SPACE_SIZE;
      if (index % spiralStep === 0) {
        turns = 0;
        spiralStep++;
      }
    }
  };

  return (
    <>
      <Center data-testid={'centerBoard'}>{points}</Center>
    </>
  );
};
