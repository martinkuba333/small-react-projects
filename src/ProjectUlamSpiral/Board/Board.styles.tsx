import styled from 'styled-components';

export const Center = styled.div`
  position: absolute;
  top: 400px;
  left: ${window.innerWidth / 2}px;
`;
