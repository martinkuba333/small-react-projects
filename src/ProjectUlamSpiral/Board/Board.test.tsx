import React from 'react';
import { render } from '@testing-library/react';
import { Board } from './Board';

describe('Board component', () => {
  it('renders correctly with points', () => {
    const startingNum = 1; // Set the starting number
    const { getByTestId, getAllByTestId } = render(<Board startingNum={startingNum} />);

    const centerElement = getByTestId('centerBoard');
    expect(centerElement).toBeInTheDocument();
  });
});
