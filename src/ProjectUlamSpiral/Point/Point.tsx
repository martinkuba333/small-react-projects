import React, { CSSProperties, useMemo, useState } from 'react';
import { PointContainer } from './Point.styles';

export interface PointInterface {
  index: number;
  top: number;
  left: number;
}

export const Point: React.FC<PointInterface> = (props: PointInterface) => {
  const numberIsPrime = (): boolean => {
    if (props.index <= 1) {
      return false;
    }
    if (props.index === 2 || props.index === 3) {
      return true;
    }
    if (props.index % 2 === 0 || props.index % 3 === 0) {
      return false;
    }

    for (let i = 3; i <= Math.sqrt(props.index); i++) {
      if (props.index % i === 0) {
        return false;
      }
    }
    return true;
  };

  const numberIsInFibonacci = (): boolean => {
    const isPerfectSquare = (n: number): boolean => {
      const sqrt = Math.sqrt(n);
      return sqrt === Math.floor(sqrt);
    };

    return (
      isPerfectSquare(5 * props.index * props.index + 4) ||
      isPerfectSquare(5 * props.index * props.index - 4)
    );
  };

  const createInlineCss = useMemo((): CSSProperties => {
    return {
      top: props.top + 'px',
      left: props.left + 'px',
      background: numberIsPrime() ? 'black' : '#efefef',
    };
  }, [props.top, props.left]);

  return (
    <>
      <PointContainer style={createInlineCss}>{props.index}</PointContainer>
    </>
  );
};
