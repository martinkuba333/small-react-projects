import React from 'react';
import { render } from '@testing-library/react';
import { Point } from './Point';

describe('Point component', () => {
  it('renders with non-prime background - 1', () => {
    const { getByText } = render(<Point index={1} top={0} left={0}></Point>);
    const pointContainer = getByText(1);
    expect(pointContainer).toHaveStyle({ top: '0px', left: '0px', background: '#efefef' });
  });

  it('renders with non-prime background - 25', () => {
    const { getByText } = render(<Point index={25} top={0} left={0}></Point>);
    const pointContainer = getByText(25);
    expect(pointContainer).toHaveStyle({ top: '0px', left: '0px', background: '#efefef' });
  });

  it('renders with prime background - 2', () => {
    const { getByText } = render(<Point index={2} top={0} left={0}></Point>);
    const pointContainer = getByText(2);
    expect(pointContainer).toHaveStyle({ top: '0px', left: '0px', background: 'black' });
  });

  it('renders with prime background - 6779', () => {
    const { getByText } = render(<Point index={6779} top={0} left={0}></Point>);
    const pointContainer = getByText(6779);
    expect(pointContainer).toHaveStyle({ top: '0px', left: '0px', background: 'black' });
  });
});
