import styled from 'styled-components';
import { CONST } from '../Utils/Constants';

export const PointContainer = styled.div`
  border-radius: 50%;
  color: white;
  position: absolute;
  width: ${Math.floor(CONST.SPACE_SIZE * 0.75)}px;
  height: ${Math.floor(CONST.SPACE_SIZE * 0.75)}px;
  font-size: 0px;
  text-align: center;

  &:hover {
    cursor: pointer;
    font-size: ${Math.floor(CONST.SPACE_SIZE / 2)}px;
  }
`;
