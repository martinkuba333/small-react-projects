import React, { useEffect, useRef, useState } from 'react';
import { Board } from './Board/Board';
import { Buttons } from './Buttons/Buttons';
import { CONST } from './Utils/Constants';
import { Title, Text } from './MainUlamSpiral.styles';

export const MainUlamSpiral: React.FC = (): JSX.Element => {
  const [startingValue, setStartingValue] = useState<number>(CONST.INITIAL_NUMBER);
  const titleRef = useRef<HTMLHeadingElement>(null);
  const textRef = useRef<HTMLParagraphElement>(null);

  useEffect(() => {
    setTimeout(() => {
      if (titleRef.current) {
        titleRef.current.style.display = 'none';
      }
      if (textRef.current) {
        textRef.current.style.display = 'none';
      }
    }, 15000);
  }, []);

  return (
    <>
      <Title ref={titleRef}>Ulam Spiral</Title>
      <Text ref={textRef}>Black dots are Prime Numbers</Text>
      <Board startingNum={startingValue} />
      <Buttons startingValue={startingValue} setStartingValue={setStartingValue} />
    </>
  );
};
