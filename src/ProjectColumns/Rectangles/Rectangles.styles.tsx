import styled from 'styled-components';

const sizes = {
  width: '800px',
  height: '700px',
};

export const Board = styled.div`
  background: #2c163d;
  ${sizes};
  overflow: hidden;
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
`;

export const RectangleContainer = styled.div`
  position: absolute;
  margin-left: 354px;
  ${sizes};
`;

export const MiscellaneousContainer = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: ${sizes.height};
`;

export const Text = styled.div`
  position: relative;
  color: black;
  top: 25px;
  left: 0;
  right: 0;
  margin: auto;
  text-align: center;
  font-size: 20px;
  font-weight: bold;
`;

export const RandomButton = styled.div`
  position: relative;
  top: 35px;
  left: 0;
  right: 0;
  margin: auto;
  border: 2px solid black;
  border-radius: 5px;
  width: fit-content;
  padding: 5px 15px;
  user-select: none;

  &:hover {
    cursor: pointer;
    color: white;
    background: black;
  }
`;
