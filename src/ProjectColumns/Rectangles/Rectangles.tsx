import React, { useEffect, useState } from 'react';
import { Rectangle } from '../Rectangle/Rectangle';
import { RectangleContainer, Board } from './Rectangles.styles';
import { CONST } from '../Utils/Const';

type RectanglePosition = {
  key: number;
  leftPos: number;
  topPos: number;
};

export interface RectanglesInterface {
  randomColorOfEveryColumn: string | null;
}

let specialCounter = 0;

export const Rectangles: React.FC<RectanglesInterface> = (props: RectanglesInterface) => {
  const [marginTop, setMarginTop] = useState<number>(CONST.RECTANGLES_STARTING_TOP);
  const [counter, setCounter] = useState<number>(0);

  useEffect(() => {
    setTimeout(() => {
      const newMarginTop = marginTop + CONST.RECTANGLES_ADD_NUM;
      setMarginTop(newMarginTop);
      moveRectanglesUp(newMarginTop);
    }, CONST.RECTANGLES_TIMER);
  }, [marginTop]);

  const moveRectanglesUp = (newMarginTop: number) => {
    if (newMarginTop % CONST.RECTANGLES_STARTING_TOP === 0) {
      const newCounter = counter + 1;
      setCounter(newCounter);
    }
  };

  const createRectangles = (): JSX.Element[] => {
    const result = [];
    specialCounter = 0;

    for (let i = 0; i < CONST.TOTAL_RECTANGLES; i++) {
      const rectangleParams = createRectangleParams(i);
      result.push(createRectangle(rectangleParams));
    }

    return result;
  };

  const createRectangleParams = (i: number): RectanglePosition => {
    let leftPos: number;
    let topPos: number;

    if (i < 4) {
      leftPos = i * 30;
      topPos = i * 30;
    } else if (i < 9) {
      const num = 4 * 30;
      leftPos = num - (i - 4) * 30;
      topPos = i * 30;
    } else if (i < 13) {
      const num = 8 * 30;
      leftPos = num - i * 30;
      topPos = num - (i - 8) * 30;
    } else {
      leftPos = -90 + specialCounter * 30;
      topPos = 0 - (specialCounter * 30 - 90);
      specialCounter++;
    }

    return {
      key: i,
      leftPos,
      topPos,
    };
  };

  const createRectangle = (rectanglePosition: RectanglePosition): JSX.Element => {
    const { key, leftPos, topPos } = rectanglePosition;
    return (
      <Rectangle
        key={key}
        index={key}
        leftPos={leftPos}
        topPos={topPos}
        counter={counter}
        randomColorOfEveryColumn={props.randomColorOfEveryColumn}
      />
    );
  };

  const createRectangleContainer = (): JSX.Element => {
    return (
      <>
        <RectangleContainer style={{ marginTop: `${marginTop}px` }}>
          {createRectangles()}
        </RectangleContainer>
      </>
    );
  };

  return (
    <>
      <Board>{createRectangleContainer()}</Board>
    </>
  );
};
