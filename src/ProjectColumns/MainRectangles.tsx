import React, { useState } from 'react';
import { Color, getRandomColor } from './Helper/Helper';
import { MiscellaneousContainer, RandomButton, Text } from './Rectangles/Rectangles.styles';
import { AppContainer } from '../App.styles';
import { Rectangles } from './Rectangles/Rectangles';

export const MainRectangles = () => {
  const [randomColorOfEveryColumn, setRandomColorOfEveryColumn] = useState<Color | null>(null);

  const randomlyChangeColor = () => {
    const randomColor = getRandomColor(randomColorOfEveryColumn);
    setRandomColorOfEveryColumn(randomColor);
  };

  const createMiscellaneousInfo = (): JSX.Element => {
    return (
      <MiscellaneousContainer>
        <Text>Make a click on column to change its color!</Text>
        <RandomButton onClick={randomlyChangeColor}>Change color of every column</RandomButton>
      </MiscellaneousContainer>
    );
  };

  return (
    <>
      <AppContainer>
        <Rectangles randomColorOfEveryColumn={randomColorOfEveryColumn} />
        {createMiscellaneousInfo()}
      </AppContainer>
    </>
  );
};
