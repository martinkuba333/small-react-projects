import styled from 'styled-components';

interface SquareProps {
  squarecolor: string | null;
}

const topColor = '#f0e9f6';
const triangleSize = '20';
const sideColorOne = '#f0e9f6';
const sideColorTwo = '#2c163d';

export const RectangleShape = styled.div<SquareProps>`
  display: inline-block;
  margin: 15px;
  position: absolute;
  transition: all 0.2s linear;

  &:hover {
    cursor: pointer;
  }
`;

const shaping = {
  width: 0,
  height: 0,
  transform: 'rotateX(40deg)',
  borderLeft: `${triangleSize}px solid transparent`,
  borderRight: `${triangleSize}px solid transparent`,
  zIndex: 150,
};

export const RectangleBottom = styled.div<SquareProps>`
  ${shaping};
  border-bottom: ${triangleSize}px solid transparent;
  border-top: ${triangleSize}px solid ${(props) => props.squarecolor || topColor};
  top: -10px;
  position: relative;
`;

export const RectangleTop = styled.div<SquareProps>`
  ${shaping};
  border-bottom: ${triangleSize}px solid ${(props) => props.squarecolor || topColor};
  border-top: ${triangleSize}px solid transparent;
  position: relative;
`;

const rectangleSides = {
  display: 'inline-block',
  width: '28px',
  height: '160px',
  zIndex: '49',
  transform: 'rotateY(45deg)',
};

export const RectangleBodyLeft = styled.div<SquareProps>`
  position: relative;
  top: -45px;
  left: -4px;
  ${rectangleSides};
  color: ${(props) => props.squarecolor};
  background: linear-gradient(
    to bottom left,
    ${(props) => props.squarecolor || sideColorOne} 20%,
    ${sideColorTwo} 80%,
    transparent 100%
  );
`;

export const RectangleBodyRight = styled.div<SquareProps>`
  position: relative;
  top: -45px;
  left: -13px;
  ${rectangleSides};
  background: linear-gradient(
    to bottom right,
    ${(props) => props.squarecolor || sideColorOne} 20%,
    ${sideColorTwo} 80%,
    transparent 100%
  );
`;
