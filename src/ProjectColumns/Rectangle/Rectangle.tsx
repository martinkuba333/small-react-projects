import React, { CSSProperties, useEffect, useState } from 'react';
import {
  RectangleTop,
  RectangleBottom,
  RectangleShape,
  RectangleBodyLeft,
  RectangleBodyRight,
} from './Rectangle.styles';
import { CONST } from '../Utils/Const';
import { Color, getRandomColor } from '../Helper/Helper';

export interface RectangleInterface {
  leftPos: number;
  topPos: number;
  index: number; //z-index?
  counter: number;
  randomColorOfEveryColumn: string | null;
}

export const Rectangle: React.FC<RectangleInterface> = (props: RectangleInterface) => {
  const [top, setTop] = useState<number>(props.topPos);
  const [squareColor, setSquareColor] = useState<Color | null>(props.randomColorOfEveryColumn);
  const bestZIndex = CONST.TOTAL_RECTANGLES / 2 + 1 > props.index;

  useEffect(() => {
    setTimeout(() => {
      const newTop = top - CONST.RECTANGLES_STARTING_TOP;
      setTop(newTop);
    }, 150 * props.index);
  }, [props.counter]);

  useEffect(() => {
    setSquareColor(props.randomColorOfEveryColumn);
  }, [props.randomColorOfEveryColumn]);

  const getRectangleInlineCss = (): CSSProperties => {
    return {
      top: `${top}px`,
      left: `${props.leftPos}px`,
      zIndex: bestZIndex ? props.index : CONST.TOTAL_RECTANGLES - props.index,
    };
  };

  const colorChange = () => {
    const randomColor = getRandomColor(squareColor);
    setSquareColor(randomColor);
  };

  return (
    <>
      <RectangleShape
        style={getRectangleInlineCss()}
        onClick={colorChange}
        squarecolor={squareColor}
      >
        <RectangleTop squarecolor={squareColor} />
        <RectangleBottom squarecolor={squareColor} />
        <RectangleBodyLeft squarecolor={squareColor} />
        <RectangleBodyRight squarecolor={squareColor} />
      </RectangleShape>
    </>
  );
};
