import styled from 'styled-components';

export const Nav = styled.nav`
  position: absolute;
  background: #efefef;
  padding: 15px 0px;
  top: 0;
  box-shadow: inset 0px 0px 5px 5px black;
  z-index: 1000;
  width: 380px;
  transition: left 0.3s linear;
  border-radius: 15px;
`;

export const NavContainer = styled.div`
  padding-left: 15px;
`;

export const Ul = styled.div`
  padding: 0;
  margin: 15px 15px 30px;
`;

export const Li = styled.li`
  width: fit-content;
  list-style-type: none;

  & > * {
    border: 1px solid rgb(85, 26, 139);
    padding: 5px 10px;
    margin: 5px;
    border-radius: 5px;
    text-decoration: none;
    display: block;
    font-weight: bold;
    width: fit-content;
    color: rgb(85, 26, 139);

    &:hover {
      cursor: pointer;
      color: white;
      background: rgb(85, 26, 139);
    }
  }
`;

export const MenuText = styled.span`
  font-weight: bold;
`;

export const Hamburger = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 36px;
  height: 27px;
  cursor: pointer;
  border: 3px solid #333;
  padding: 5px 3px;
  border-radius: 9px;
  position: absolute;
  z-index: 2;
  top: 5px;
  left: calc(100% + 5px);
  box-shadow: inset 0px 0px 0px 500px white;
`;

export const Line = styled.div`
  width: 80%;
  height: 3px;
  background-color: #333;
  margin: auto;
  transition: transform 0.3s ease;
`;

export const X = styled.div`
  text-align: center;
  font-weight: bold;
  font-size: 33px;
  position: absolute;
  top: 0px;
  left: 10px;
  pointer-events: none;
  font-family: sans-serif;
`;
