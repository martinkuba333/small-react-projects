import React, { useState } from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import { Li, Nav, Ul, Hamburger, Line, X, NavContainer, MenuText } from './Links.styles';
import { myInsideRoutes, RouteTypes } from '../InsideLinks/InsideLinks';
import { createOutsideLinks, createPersonalLinks } from '../OutsideLinks/OutsideLinks';

export const Links: React.FC = (): JSX.Element => {
  const [menuIsHidden, setMenuIsHidden] = useState<boolean>(true);

  const createRoutes = (): JSX.Element => {
    return (
      <Routes>
        {myInsideRoutes.map((route: RouteTypes) => (
          <Route key={route.url} path={route.url} element={route.element} />
        ))}
      </Routes>
    );
  };

  const createInsideLinks = (): JSX.Element => {
    return (
      <>
        {myInsideRoutes.map((route: RouteTypes) => (
          <Li key={route.url} onClick={switchSidebarVisibility}>
            <Link to={route.url}>{route.text}</Link>
          </Li>
        ))}
      </>
    );
  };

  const switchSidebarVisibility = () => {
    setMenuIsHidden(!menuIsHidden);
  };

  const createHamburgerMenu = (): JSX.Element => {
    const titleHover = 'Open/Close menu';

    return (
      <>
        <Hamburger onClick={switchSidebarVisibility} title={titleHover}>
          {menuIsHidden ? (
            <>
              <Line />
              <Line />
              <Line />
            </>
          ) : (
            <X>X</X>
          )}
        </Hamburger>
      </>
    );
  };

  const createMenu = (): JSX.Element => {
    return (
      <>
        <MenuText>List of Projects</MenuText>
        <Ul>{createInsideLinks()}</Ul>
        <Ul>{createOutsideLinks()}</Ul>
        <MenuText>About Me</MenuText>
        <Ul>{createPersonalLinks()}</Ul>
      </>
    );
  };

  return (
    <>
      <Nav style={menuIsHidden ? { left: '-380px' } : { left: '0' }}>
        {createHamburgerMenu()}
        <NavContainer>{createMenu()}</NavContainer>
      </Nav>
      {createRoutes()}
    </>
  );
};
