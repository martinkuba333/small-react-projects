import React from 'react';
import { MainRectangles } from '../ProjectColumns/MainRectangles';
import { MainPurpleRain } from '../ProjectPurpleRain/MainPurpleRain';
import { MainPrint } from '../ProjectPrint10/MainPrint';
import { MainCarpets } from '../ProjectCarpets/MainCarpets';
import { MainWeaves } from '../ProjectWeaves/MainWeaves';
import { MainUlamSpiral } from '../ProjectUlamSpiral/MainUlamSpiral';
import { MainSlidePuzzle } from '../ProjectSlidePuzzle/MainSlidePuzzle';
import { MainCircles } from '../ProjectCircles/MainCircles';
import { MainPhyllotaxis } from '../ProjectPhyllotaxis/MainPhyllotaxis';
import { MainDodgeGame } from '../ProjectDodgeGame/MainDodgeGame';

export type RouteTypes = {
  url: string;
  element: JSX.Element;
  text: string;
};

export const myInsideRoutes: RouteTypes[] = [
  {
    url: '/',
    element: <UlamSpiral />,
    text: 'Ulam spiral in React/Typescript',
  },
  {
    url: '/rectangle-animation',
    element: <RectangleAnimation />,
    text: 'Rectangle Animation in React/Typescript',
  },
  {
    url: '/purple-rain',
    element: <PurpleRain />,
    text: 'Purple Rain in React/Typescript',
  },
  {
    url: '/print-10',
    element: <Print />,
    text: '10 Print in React/Typescript',
  },
  {
    url: '/carpet',
    element: <Carpet />,
    text: 'Flying squares in React/Typescript',
  },
  {
    url: '/weaves',
    element: <Weaves />,
    text: 'Weaves in React/Typescript',
  },
  {
    url: '/slide-puzzle',
    element: <SlidePuzzle />,
    text: 'Slide Puzzle in React/Typescript',
  },
  {
    url: '/circles',
    element: <Circles />,
    text: 'Circles in React/Typescript',
  },
  {
    url: '/phyllotaxis',
    element: <Phyllotaxis />,
    text: 'Phyllotaxis in React/Typescript',
  },
  {
    url: '/dodge-game',
    element: <DodgeGame />,
    text: 'Dodge Game in React',
  },
];

function RectangleAnimation(): JSX.Element {
  return (
    <>
      <MainRectangles />
    </>
  );
}

function PurpleRain(): JSX.Element {
  return (
    <>
      <MainPurpleRain />
    </>
  );
}

function Print(): JSX.Element {
  return (
    <>
      <MainPrint />
    </>
  );
}

function Carpet(): JSX.Element {
  return (
    <>
      <MainCarpets />
    </>
  );
}

function Weaves(): JSX.Element {
  return (
    <>
      <MainWeaves />
    </>
  );
}

function UlamSpiral(): JSX.Element {
  return (
    <>
      <MainUlamSpiral />
    </>
  );
}

function SlidePuzzle(): JSX.Element {
  return (
    <>
      <MainSlidePuzzle />
    </>
  );
}

function Circles(): JSX.Element {
  return (
    <>
      <MainCircles />
    </>
  );
}

function Phyllotaxis(): JSX.Element {
  return (
    <>
      <MainPhyllotaxis />
    </>
  );
}

function DodgeGame(): JSX.Element {
  return (
    <>
      <MainDodgeGame />
    </>
  );
}
