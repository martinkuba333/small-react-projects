import React, { Component } from 'react';
import styled from 'styled-components';

const squareSize = 20;
const boardSize = squareSize * 11;

const Seconds = styled.div`
  text-align: center;
`;

const Board = styled.section`
  margin: 15px auto;
  width: ${boardSize}px;
  height: ${boardSize}px;
  background: grey;
  position: relative;
  overflow: hidden;
`;

const Player = styled.div`
  width: ${squareSize}px;
  height: ${squareSize}px;
  background: yellow;
  position: absolute;
  left: ${boardSize / 2 - squareSize / 2}px;
  top: ${boardSize / 2 - squareSize / 2}px;
`;

const Start = styled.button`
  margin: 20px auto;
  display: block;
`;

const enemyAttr = 'move';
let gameTimer;
let enemyInterval;
let enemySpeedInterval = 50;
let enemySpeed = enemySpeedInterval / 14;

class Container extends Component {
  constructor() {
    super();
    this.state = {
      numOfEnemies: 1,
      removeEnemies: 0,
    };
  }

  startGame = (e) => {
    e.target.parentNode.removeChild(e.target);
    document.onkeydown = this.myEvent;
    this.startTimer();
    this.attack();
  };

  startTimer = () => {
    const seconds = this.refs.refSeconds;
    const self = this;

    gameTimer = setInterval(function () {
      seconds.innerText++;
      if (seconds.innerText % 10 === 0) {
        self.state.numOfEnemies++;
        self.state.removeEnemies++;
      }
    }, 1000);
  };

  resetTimer = () => {
    this.refs.refSeconds.innerText = 0;
  };

  myEvent = (e) => {
    if (e.key === 'w' /*|| e.key === "ArrowUp"*/) this.movePlayer('up');
    if (e.key === 's' /*|| e.key === "ArrowDown"*/) this.movePlayer('down');
    if (e.key === 'a' /*|| e.key === "ArrowLeft"*/) this.movePlayer('left');
    if (e.key === 'd' /*|| e.key === "ArrowRight"*/) this.movePlayer('right');
  };

  placePlayerToMiddle = () => {
    const player = this.refs.refPlayer;
    player.style.left = boardSize / 2 - squareSize / 2 + 'px';
    player.style.top = boardSize / 2 - squareSize / 2 + 'px';
  };

  movePlayer = (dir) => {
    const player = this.refs.refPlayer;
    const left = parseInt(window.getComputedStyle(player).left);
    const top = parseInt(window.getComputedStyle(player).top);

    if (
      (left > 0 && left < boardSize - squareSize) ||
      (left === 0 && dir === 'right') ||
      (left === boardSize - squareSize && dir === 'left')
    ) {
      player.style.left =
        left + (dir === 'left' ? -squareSize : dir === 'right' ? squareSize : 0) + 'px';
    }

    if (
      (top > 0 && top < boardSize - squareSize) ||
      (top === 0 && dir === 'down') ||
      (top === boardSize - squareSize && dir === 'up')
    ) {
      player.style.top =
        top + (dir === 'up' ? -squareSize : dir === 'down' ? squareSize : 0) + 'px';
    }
  };

  attack = () => {
    this.createEnemies();
    this.prepareMoveEnemies();
  };

  createEnemies = () => {
    const board = this.refs.refBoard;
    const self = this;

    let controlCounter = 0;

    let creationInterval = setInterval(function () {
      controlCounter++;
      self.createEnemy(board);
      if (controlCounter === self.state.numOfEnemies) {
        clearInterval(creationInterval);
      }
    }, 400);
  };

  createEnemy = (board) => {
    const player = this.refs.refPlayer;
    const plLeft = parseInt(window.getComputedStyle(player).left);
    const plTop = parseInt(window.getComputedStyle(player).top);

    let enemy = document.createElement('div');

    enemy.classList += 'enemy';
    enemy.style.width = squareSize + 'px';
    enemy.style.height = squareSize + 'px';
    enemy.style.background = 'red';
    enemy.style.position = 'absolute';

    this.styleEnemyPosition(enemy, plLeft, plTop);

    board.appendChild(enemy);
  };

  styleEnemyPosition = (enemy, plLeft, plTop) => {
    const rand = Math.floor(Math.random() * 4);

    switch (rand) {
      case 0:
        enemy.style.left = plLeft + 'px';
        enemy.style.top = 0 + 'px';
        enemy.setAttribute(enemyAttr, 'down');
        break;
      case 1:
        enemy.style.left = 200 + 'px';
        enemy.style.top = plTop + 'px';
        enemy.setAttribute(enemyAttr, 'left');
        break;
      case 2:
        enemy.style.left = plLeft + 'px';
        enemy.style.top = 200 + 'px';
        enemy.setAttribute(enemyAttr, 'up');
        break;
      case 3:
        enemy.style.left = 0 + 'px';
        enemy.style.top = plTop + 'px';
        enemy.setAttribute(enemyAttr, 'right');
        break;
    }
  };

  prepareMoveEnemies = () => {
    const self = this;

    enemyInterval = setInterval(function () {
      const enemies = document.getElementsByClassName('enemy');

      for (let i = 0; i < enemies.length; i++) {
        let en = enemies[i];
        let left = parseInt(window.getComputedStyle(en).left);
        let top = parseInt(window.getComputedStyle(en).top);

        self.checkBorderHit(en, left, top);
        self.checkPlayerHit(left, top);
        self.moveEnemies(en, left, top);
      }
    }, enemySpeedInterval);
  };

  moveEnemies = (en, left, top) => {
    switch (en.getAttribute(enemyAttr)) {
      case 'down':
        en.style.top = top + enemySpeed + 'px';
        break;
      case 'left':
        en.style.left = left - enemySpeed + 'px';
        break;
      case 'up':
        en.style.top = top - enemySpeed + 'px';
        break;
      case 'right':
        en.style.left = left + enemySpeed + 'px';
        break;
    }
  };

  checkBorderHit = (en, left, top) => {
    if (
      left < 0 - squareSize ||
      left > boardSize + squareSize ||
      top < 0 - squareSize ||
      top > boardSize + squareSize
    ) {
      this.removeElement(en);
      this.state.removeEnemies++;
      this.checkIfNewEnemiesShouldBeCreated();
    }
  };

  checkIfNewEnemiesShouldBeCreated = () => {
    if (this.state.removeEnemies === this.state.numOfEnemies) {
      this.state.removeEnemies = 0;

      const self = this;

      setTimeout(function () {
        self.createEnemies();
      }, 100);
    }
  };

  checkPlayerHit = (enLeft, enTop) => {
    const player = this.refs.refPlayer;
    let plLeft = parseInt(window.getComputedStyle(player).left);
    let plTop = parseInt(window.getComputedStyle(player).top);

    if (
      plLeft < enLeft + squareSize &&
      plLeft + squareSize > enLeft &&
      plTop < enTop + squareSize &&
      plTop + squareSize > enTop
    ) {
      this.gameRestart();
    }
  };

  removeEnemies = () => {
    const enemies = document.getElementsByClassName('enemy');

    while (enemies[0]) {
      this.removeElement(enemies[0]);
    }

    this.setState({
      numOfEnemies: 1,
      removeEnemies: 0,
    });
  };

  removeElement = (ele) => {
    ele.parentNode.removeChild(ele);
  };

  gameRestart = () => {
    const self = this;

    this.removeEnemies();
    this.resetTimer();
    this.placePlayerToMiddle();

    setTimeout(function () {
      self.createEnemies();
    }, 100);
  };

  render() {
    return (
      <React.Fragment>
        <Seconds>
          Seconds alive: <span ref="refSeconds">0</span>
        </Seconds>
        <Board ref="refBoard">
          <Player ref="refPlayer" />
        </Board>
        <Start onClick={this.startGame}>Start</Start>
      </React.Fragment>
    );
  }
}

export const MainDodgeGame = (): JSX.Element => {
  return (
    <>
      <h3 style={{ textAlign: 'center' }}>DODGE GAME</h3>
      <Container />
      <p style={{ textAlign: 'center' }}>Use WSAD to move</p>
      <p style={{ textAlign: 'center' }}>
        Avoid yellow squares and survive as many seconds as possible
      </p>
    </>
  );
};
