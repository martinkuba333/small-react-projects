import React, { useState } from 'react';
import { Board } from './Board/Board';
import { ColorChanger } from './MainPurpleRain.styles';
import { Color, getRandomColor } from './Helper/Helper';
import { CONST } from './Utils/Constants';

export const MainPurpleRain: React.FC = (): JSX.Element => {
  const [dropletColor, setDropletColor] = useState<Color>(CONST.DEFAULT_COLOR);

  const changeRainColor = () => {
    setDropletColor(getRandomColor(dropletColor));
  };

  return (
    <>
      <Board dropletColor={dropletColor} />
      <p style={{ textAlign: 'center', color: `${CONST.DEFAULT_COLOR}` }}>
        Rain in React + Typescript
      </p>
      <ColorChanger onClick={changeRainColor}>Randomly change color</ColorChanger>
    </>
  );
};
