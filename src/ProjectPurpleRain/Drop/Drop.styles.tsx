import styled from 'styled-components';

export const Droplet = styled.div`
  position: absolute;
  border-radius: 5px;
`;
