import React, { CSSProperties } from 'react';
import { Droplet } from './Drop.styles';
import { CONST } from '../Utils/Constants';
import { Color } from '../Helper/Helper';

interface DropInterface {
  dropletTop: number;
  dropletLeft: number;
  dropletSpeedAndWidth: number;
  dropletColor: Color;
}

export const Drop: React.FC<DropInterface> = (props: DropInterface) => {
  const createDropletInlineStyle = (): CSSProperties => {
    return {
      top: `${props.dropletTop}px`,
      left: `${props.dropletLeft}px`,
      width: `${props.dropletSpeedAndWidth}px`,
      height: `${props.dropletSpeedAndWidth * CONST.DROP_HEIGHT_WIDTH_RATIO}px`,
      background: `${props.dropletColor}`,
    };
  };

  return (
    <>
      <Droplet style={createDropletInlineStyle()} />
    </>
  );
};
