import styled from 'styled-components';
import { CONST } from './Utils/Constants';

export const ColorChanger = styled.div`
  border: 2px solid ${CONST.DEFAULT_COLOR};
  text-align: center;
  width: fit-content;
  margin: auto;
  padding: 5px 15px;
  border-radius: 5px;
  color: ${CONST.DEFAULT_COLOR};

  &:hover {
    color: white;
    background: ${CONST.DEFAULT_COLOR};
    user-select: none;
    cursor: pointer;
  }
`;
