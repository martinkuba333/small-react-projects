export const COLORS = [
  'red',
  'green',
  'lightblue',
  'gold',
  'white',
  'yellow',
  'pink',
  'orange',
  'purple',
  'cyan',
  'silver',
  'blue',
  'black',
];
export type Color = (typeof COLORS)[number];

export const getRandomColor = (color: Color | null): Color => {
  let randomIndex = Math.floor(Math.random() * COLORS.length);

  // If color is provided and it matches the randomly generated color, keep generating until a new color is found
  while (color && COLORS[randomIndex] === color) {
    randomIndex = Math.floor(Math.random() * COLORS.length);
  }

  return COLORS[randomIndex];
};
