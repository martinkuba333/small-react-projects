import React, { useEffect, useState } from 'react';
import { Area } from './Board.styles';
import { Drop } from '../Drop/Drop';
import { CONST } from '../Utils/Constants';
import { Color } from '../Helper/Helper';

interface DropsValuesInterface {
  dropletTop: number;
  dropletLeft: number;
  dropletSpeedAndWidth: number;
}

interface BoardInterface {
  dropletColor: Color;
}

interface UpdatedDropValues {
  dropletLeft: number;
  dropletSpeedAndWidth: number;
  dropletTop: number;
}

export const Board: React.FC<BoardInterface> = (props: BoardInterface) => {
  const [dropsValues, setDropsValues] = useState<DropsValuesInterface[]>([]);
  const [drops, setDrops] = useState<JSX.Element[]>([]);

  useEffect(() => {
    const resultDropsValues = [];
    for (let i = 0; i < CONST.SUM_OF_DROPS; i++) {
      resultDropsValues.push({
        dropletTop: getRandomStartTop(),
        dropletLeft: getRandomLeft(),
        dropletSpeedAndWidth: getRandomSpeedAndWidth(),
      });
    }

    setDropsValues(resultDropsValues);
  }, []);

  useEffect(() => {
    const intervalId = setInterval(() => {
      const resultDrops: JSX.Element[] = [];
      const updatedDropsValues: DropsValuesInterface[] = [];

      dropsValues.forEach((dropValue: DropsValuesInterface, i: number) => {
        const { dropletTop, dropletSpeedAndWidth, dropletLeft } = prepareDropCreation(dropValue);
        resultDrops.push(
          <Drop
            key={i}
            dropletTop={dropletTop}
            dropletLeft={dropletLeft}
            dropletSpeedAndWidth={dropletSpeedAndWidth}
            dropletColor={props.dropletColor}
          />,
        );
        updatedDropsValues.push({ dropletTop, dropletSpeedAndWidth, dropletLeft });
      });

      if (updatedDropsValues.length) {
        setDropsValues(updatedDropsValues);
      }
      setDrops(resultDrops);
    }, CONST.RAIN_INTERVAL);
    return () => clearInterval(intervalId);
  }, [drops]);

  const prepareDropCreation = (dropValue: DropsValuesInterface): UpdatedDropValues => {
    let dropletTop = dropValue.dropletTop + dropValue.dropletSpeedAndWidth;
    let dropletSpeedAndWidth = dropValue.dropletSpeedAndWidth;
    let dropletLeft = dropValue.dropletLeft;

    if (dropletTop > CONST.BOARD_HEIGHT + CONST.DROP_DIS_APPEAR_POS) {
      dropletTop = -CONST.DROP_DIS_APPEAR_POS;
      dropletSpeedAndWidth = getRandomSpeedAndWidth();
      dropletLeft = getRandomLeft();
    }

    return { dropletTop, dropletSpeedAndWidth, dropletLeft };
  };

  const getRandomLeft = (): number => {
    return Math.floor(Math.random() * CONST.BOARD_WIDTH);
  };

  const getRandomSpeedAndWidth = (): number => {
    return Math.floor(Math.random() * CONST.MAX_SPEED) + CONST.MIN_SPEED;
  };

  const getRandomStartTop = (): number => {
    return Math.floor(Math.random() * CONST.BOARD_HEIGHT) - CONST.BOARD_HEIGHT * 2;
  };

  return (
    <>
      <Area>{drops}</Area>
    </>
  );
};
