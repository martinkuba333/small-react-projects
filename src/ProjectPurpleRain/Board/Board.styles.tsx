import styled from 'styled-components';
import { CONST } from '../Utils/Constants';

export const Area = styled.div`
  width: ${CONST.BOARD_WIDTH}px;
  height: ${CONST.BOARD_HEIGHT}px;
  border: 2px solid black;
  margin: 0px auto;
  background: #eee;
  position: relative;
  overflow: hidden;
`;
