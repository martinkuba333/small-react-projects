import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  value: 0,
};

const ReduxState = createSlice({
  name: 'click',
  initialState,
  reducers: {
    incrementClick: (state) => {
      state.value += 1;
    },
  },
});

export const { incrementClick } = ReduxState.actions;
export default ReduxState.reducer;
