import styled from 'styled-components';

export const AppContainer = styled.div`
  margin: auto;
  position: absolute;
  left: 0;
  right: 0;
`;
