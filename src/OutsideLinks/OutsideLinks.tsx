import React from 'react';
import { Li } from '../Links/Links.styles';

interface Link {
  url: string;
  name: string;
  key: number;
}
interface OutsideLink extends Link {}
interface PersonalLink extends Link {}

const createOutsideLink = (link: OutsideLink | PersonalLink): JSX.Element => {
  return (
    <Li key={link.key}>
      <a href={link.url} target="_blank" rel="noreferrer" key={link.key}>
        {link.name}
      </a>
    </Li>
  );
};

export const createOutsideLinks = (): JSX.Element => {
  let outsideIndexCounter = 0;
  const outsideLinks: OutsideLink[] = [
    {
      url: 'https://react-sand-falling-martinkuba333-be378ad70a7d4b3f0b9595b0b02829.gitlab.io/',
      name: 'Falling sand simulation in React/Typescript',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://martinkuba333.gitlab.io/tower-defense/',
      name: 'Tower Defense game in React',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://rockpaperscissors-martinkuba333-63accff5106d4e9e04de5fd6616d133.gitlab.io/',
      name: 'Rock Paper Scissors in React',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://martinkuba333.gitlab.io/color-switcher/',
      name: 'Color Switcher game in React',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://martinkuba333.gitlab.io/game-2048/',
      name: 'Game 2048 in React',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://play.google.com/store/apps/details?id=com.toastmasters.timer&hl=en_US',
      name: 'Timer for Toastmasters in React Native',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://martinkuba333.gitlab.io/mastermind-pure-js/',
      name: 'Game Mastermind in Vanilla JS',
      key: outsideIndexCounter++,
    },
    {
      url: 'https://martinkuba333.gitlab.io/frogger/',
      name: 'Frogger game in React',
      key: outsideIndexCounter++,
    },
  ];

  return <>{outsideLinks.map((link: OutsideLink) => createOutsideLink(link))}</>;
};

export const createPersonalLinks = (): JSX.Element => {
  let personalIndexCounter = 0;
  const personalLinks: PersonalLink[] = [
    {
      url: 'https://www.linkedin.com/in/martin-kuba-82b267bb/',
      name: 'LINKEDIN',
      key: personalIndexCounter++,
    },
    {
      url: 'https://stackoverflow.com/users/7473844/martin54',
      name: 'STACKOVERFLOW',
      key: personalIndexCounter++,
    },
    {
      url: 'https://www.codingame.com/profile/4c8b84ec928415a2a7452737a9aa36231139151',
      name: 'CODINGAME',
      key: personalIndexCounter++,
    },
    {
      url: 'https://www.linkedin.com/in/martin-kuba-82b267bb/details/certifications/',
      name: 'CERTIFICATES',
      key: personalIndexCounter++,
    },
  ];

  return <>{personalLinks.map((link: PersonalLink) => createOutsideLink(link))}</>;
};
