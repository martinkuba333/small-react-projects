import React, { useEffect, useState } from 'react';
import { Area } from './Board.styles';
import { Square } from '../Square/Square';
import { CONST } from '../Utils/Constants';
import { Miscellaneous } from '../Miscellaneous/Miscellaneous';
import { Color } from '../Helper/Helper';

interface SquaresValues {
  key: number;
  rightRotation: boolean;
}

export const Board: React.FC = () => {
  const [counter, setCounter] = useState<number>(0);
  const [width, setWidth] = useState<number>(CONST.LINE_WIDTH);
  const [bgColor, setBgColor] = useState<Color>('white');
  const [probabilityDir, setProbabilityDir] = useState<number>(CONST.RIGHT_PROBABILITY_START);
  const [switchDirection, setSwitchDirection] = useState<boolean>(false);
  const [squaresValues, setSquaresValues] = useState<SquaresValues[]>([]);

  useEffect(() => {
    const squareInterval = setInterval(() => {
      if (
        counter <
        CONST.BOARD_MULTIPLICATION_SIZE * CONST.BOARD_MULTIPLICATION_SIZE +
          CONST.BOARD_MULTIPLICATION_SIZE * 2
      ) {
        createNewSquaresValues();
      }
    }, CONST.INTERVAL);
    return () => clearTimeout(squareInterval);
  }, [counter]);

  // useEffect(() => {
  //   restart()
  // }, [probabilityDir])

  const createSquares = (): JSX.Element[] => {
    return squaresValues.map((square: SquaresValues) => (
      <Square
        key={square.key}
        isRotatingRight={square.rightRotation}
        thickness={width}
        background={bgColor}
        switchDir={switchDirection}
      />
    ));
  };

  const createNewSquaresValues = () => {
    const newCounter = counter + 1;
    const newSquaresValues = [...squaresValues];
    const rightRotation = Math.floor(Math.random() * 100) < probabilityDir;
    newSquaresValues.push({
      key: newCounter,
      rightRotation,
    });
    setSquaresValues(newSquaresValues);
    setCounter(newCounter);
  };

  const restart = () => {
    setSquaresValues([]);
    setCounter(0);
  };

  return (
    <>
      <Area>{createSquares()}</Area>
      <Miscellaneous
        restart={restart}
        width={width}
        setWidth={setWidth}
        bgColor={bgColor}
        setBgColor={setBgColor}
        probabilityDir={probabilityDir}
        setProbabilityDir={setProbabilityDir}
        switchDir={switchDirection}
        setSwitchDir={setSwitchDirection}
      />
    </>
  );
};
