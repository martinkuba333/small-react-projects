import styled from 'styled-components';
import { CONST } from '../Utils/Constants';

export const Area = styled.div`
  display: block;
  margin: 15px auto;
  background: black;
  width: ${CONST.SQUARE_SIZE * CONST.BOARD_MULTIPLICATION_SIZE}px;
  height: ${CONST.SQUARE_SIZE * CONST.BOARD_MULTIPLICATION_SIZE}px;
  color: white;
  outline: 3px solid black;
  padding-top: 5px;
  overflow: hidden;
`;
