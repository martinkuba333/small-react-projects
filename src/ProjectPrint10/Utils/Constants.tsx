export const CONST = {
  INTERVAL: 25,
  SQUARE_SIZE: 20,
  BOARD_MULTIPLICATION_SIZE: 40,
  RANDOM_ROTATION: 0.5,
  LINE_WIDTH: 2,
  LINE_MAX_WIDTH: 5,
  LINE_MIN_WIDTH: 1,
  RIGHT_PROBABILITY_START: 50,
};

//TODO ideas
// add border
// border but transparent inside
// different images - circles vs squares?
