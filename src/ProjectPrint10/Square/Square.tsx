import React, { useEffect, useState } from 'react';
import { SquareLine, SquareVisual } from './Square.styles';

interface SquareInterface {
  isRotatingRight: boolean;
  thickness: number;
  background: string;
  switchDir: boolean;
}

interface InlineCss {
  background: string;
  transform: string;
  marginTop: string;
  outline: string;
  width: string;
}

export const Square: React.FC<SquareInterface> = (props: SquareInterface) => {
  const [isRotatingRight, setIsRotatingRight] = useState<boolean>(props.isRotatingRight);

  useEffect(() => {
    const newIsRotatingRight = !isRotatingRight;
    setIsRotatingRight(newIsRotatingRight);
  }, [props.switchDir]);

  const createInlineCss = (): InlineCss => {
    return {
      ...createBg(),
      ...createRandomRotation(),
      ...createRandomBorder(),
      ...createWidth(),
    };
  };

  const createWidth = (): Pick<InlineCss, 'width'> => {
    return { width: `${props.thickness}px` };
  };

  const createRandomBorder = (): Pick<InlineCss, 'outline'> => {
    const randomBorder = Math.random() < 0.5;
    return randomBorder
      ? {
          outline: '0px solid transparent',
        }
      : {
          outline: '0px solid transparent',
        };
  };

  const createBg = (): Pick<InlineCss, 'background'> => {
    return { background: `${props.background}` };
  };

  const createRandomRotation = (): Pick<InlineCss, 'transform' | 'marginTop'> => {
    return isRotatingRight
      ? {
          transform: `${props.switchDir ? 'rotate(45deg)' : 'rotate(-45deg)'}`,
          marginTop: `${props.switchDir ? '-7px' : '-23px'}`,
        }
      : {
          transform: `${!props.switchDir ? 'rotate(45deg)' : 'rotate(-45deg)'}`,
          marginTop: `${!props.switchDir ? '-7px' : '-23px'}`,
        };
  };

  return (
    <>
      <SquareVisual>
        <SquareLine style={createInlineCss()} />
      </SquareVisual>
    </>
  );
};
