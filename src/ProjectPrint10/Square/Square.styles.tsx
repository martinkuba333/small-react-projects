import styled from 'styled-components';
import { CONST } from '../Utils/Constants';

export const SquareVisual = styled.div`
  background: transparent;
  width: ${CONST.SQUARE_SIZE}px;
  height: ${CONST.SQUARE_SIZE}px;
  display: inline-block;
  overflow: hidden;
  margin-top: -5px;
`;

export const SquareLine = styled.div`
  height: 50px;
  margin-left: 0px;
  transition:
    color,
    width 0.5s linear;
`;
