import styled from 'styled-components';

export const ButtonContainer = styled.div`
  width: fit-content;
  margin: auto;
`;

export const Button = styled.div`
  border: 2px solid black;
  text-align: center;
  width: fit-content;
  margin: 5px;
  padding: 5px 15px;
  border-radius: 5px;
  color: black;
  display: inline-block;
  user-select: none;

  &:hover {
    color: white;
    background: black;
    user-select: none;
    cursor: pointer;
  }
`;
