import React from 'react';
import { Button, ButtonContainer } from './Miscellaneous.styles';
import { CONST } from '../Utils/Constants';
import { Color, getRandomColor } from '../Helper/Helper';

interface MiscellaneousInterface {
  restart: () => void;
  width: number;
  setWidth: (width: number) => void;
  setBgColor: (color: Color) => void;
  bgColor: Color;
  probabilityDir: number;
  setProbabilityDir: (dir: number) => void;
  switchDir: boolean;
  setSwitchDir: (newSwitchDir: boolean) => void;
}

export const Miscellaneous: React.FC<MiscellaneousInterface> = (props: MiscellaneousInterface) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setWidth(parseInt(event.target.value));
  };

  const changeBgColor = () => {
    props.setBgColor(getRandomColor(props.bgColor));
  };

  const probabilityDirChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    props.setProbabilityDir(parseInt(event.target.value));
  };

  const changeDirection = () => {
    const newSwitchDir = !props.switchDir;
    props.setSwitchDir(newSwitchDir);
  };

  const createThicknessSlider = (): JSX.Element => {
    return (
      <div>
        <label>Thickness: {props.width}</label>
        <br />
        <input
          type="range"
          id="thicknessSlider"
          name="thicknessSlider"
          min={CONST.LINE_MIN_WIDTH}
          max={CONST.LINE_MAX_WIDTH}
          step={1}
          value={props.width}
          onChange={handleChange}
        />
      </div>
    );
  };

  const createProbabilitySlider = (): JSX.Element => {
    return (
      <div>
        <label>Direction to Right Probability: {props.probabilityDir}%</label>
        <br />
        <input
          type="range"
          id="probabilitySlider"
          name="probabilitySlider"
          min={0}
          max={100} //%
          step={5}
          value={props.probabilityDir}
          onChange={probabilityDirChange}
        />
      </div>
    );
  };

  const createConfigurationButtons = (): JSX.Element => {
    return (
      <>
        <Button onClick={changeBgColor}>Change Color</Button>
        <Button onClick={changeDirection}>Switch direction (In progress)</Button>
        <Button onClick={props.restart}>Clean Screen</Button>
      </>
    );
  };

  return (
    <>
      <ButtonContainer>
        {createConfigurationButtons()}
        {createThicknessSlider()}
        {createProbabilitySlider()}
      </ButtonContainer>
    </>
  );
};
