import styled from 'styled-components';
import { CONST } from '../Utils/Constants';

export const ImageSlice = styled.div`
  position: absolute;
  width: ${CONST.SQUARES_WIDTH}px;
  height: ${CONST.SQUARES_WIDTH}px;
  background-image: url('/images/${CONST.IMAGE}');
  background-size: ${CONST.SQUARES_WIDTH * CONST.SQUARES_SUM}px
    ${CONST.SQUARES_WIDTH * CONST.SQUARES_SUM}px;
  pointer-events: none;
`;
