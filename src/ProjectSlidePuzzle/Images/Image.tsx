import React, { CSSProperties, useEffect, useState } from 'react';
import { ImageSlice } from './Image.styles';
import { createBgPositionPoints } from '../Helper/Helper';
import { CONST } from '../Utils/Constants';

interface ImagesInterface {
  up: boolean;
  left: boolean;
  right: boolean;
  down: boolean;
  shuffle: boolean;
  setUp: (value: boolean) => void;
  setLeft: (value: boolean) => void;
  setRight: (value: boolean) => void;
  setDown: (value: boolean) => void;
  setShuffle: (value: boolean) => void;
}

export type PositionType = {
  x: number;
  y: number;
  index: number;
};

type MappedImagesType = {
  index: number;
  position: PositionType;
};

const propsTimeout = 100;

export const Images: React.FC<ImagesInterface> = (props: ImagesInterface): JSX.Element => {
  const [slicedImages, setSlicedImages] = useState<MappedImagesType[]>([]);
  const [indexOfEmptyImage, setIndexOfEmptyImage] = useState<number>(-1);

  let top = 0;
  let left = 0;

  useEffect(() => {
    const backgroundPositions: PositionType[] = createBgPositionPoints();

    const mappedImages: MappedImagesType[] = backgroundPositions.map(
      (position: PositionType, index: number) => ({
        position: position,
        index: index,
      }),
    );
    const shuffledImages = shuffleArrayExceptLastIndex(mappedImages);
    setSlicedImages(shuffledImages);
  }, []);

  useEffect(() => {
    if (props.up && indexOfEmptyImage >= CONST.SQUARES_SUM) {
      handleMovement(-CONST.SQUARES_SUM);
    }
    if (props.left && indexOfEmptyImage % CONST.SQUARES_SUM !== 0) {
      handleMovement(-1);
    }
    if (props.right && (indexOfEmptyImage + 1) % CONST.SQUARES_SUM !== 0) {
      handleMovement(1);
    }
    if (
      props.down &&
      indexOfEmptyImage < CONST.SQUARES_SUM * CONST.SQUARES_SUM - CONST.SQUARES_SUM
    ) {
      handleMovement(CONST.SQUARES_SUM);
    }
    if (props.shuffle) {
      processShuffle();
    }
    setPropsToFalse();
  }, [props]);

  const processShuffle = () => {
    const sortedImages = [...slicedImages];
    sortedImages.sort((a, b) => a.index - b.index);
    const shuffledImages = shuffleArrayExceptLastIndex(sortedImages);
    setSlicedImages(shuffledImages);
    setIndexOfEmptyImage(CONST.SQUARES_SUM * CONST.SQUARES_SUM - 1);
  };

  const setPropsToFalse = () => {
    setTimeout(() => {
      props.setUp(false);
      props.setDown(false);
      props.setRight(false);
      props.setLeft(false);
      props.setShuffle(false);
    }, propsTimeout);
  };

  const shuffleArrayExceptLastIndex = (normalArray: MappedImagesType[]): MappedImagesType[] => {
    const shuffledArray = [...normalArray];
    for (let i = shuffledArray.length - 2; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
    }
    return shuffledArray;
  };

  const handleMovement = (moveByNumber: number) => {
    const indexOfFullImage = indexOfEmptyImage + moveByNumber;
    const newSlicedImages = [...slicedImages];
    [newSlicedImages[indexOfFullImage], newSlicedImages[indexOfEmptyImage]] = [
      newSlicedImages[indexOfEmptyImage],
      newSlicedImages[indexOfFullImage],
    ];
    setSlicedImages(newSlicedImages);
    const newIndexOfEmptyImage = indexOfEmptyImage + moveByNumber;
    setIndexOfEmptyImage(newIndexOfEmptyImage);
  };

  const createInlineCss = (
    position: PositionType,
    realIndex: number,
    fakeIndex: number,
  ): CSSProperties => {
    return {
      ...calculateInitialPosition(position, realIndex),
      ...hideLast(position, fakeIndex),
    };
  };

  const hideLast = (position: PositionType, index: number): CSSProperties | void => {
    const isEmpty = position.index === CONST.SQUARES_SUM * CONST.SQUARES_SUM - 1;
    if (isEmpty && indexOfEmptyImage === -1) {
      setIndexOfEmptyImage(CONST.SQUARES_SUM * CONST.SQUARES_SUM - 1);
    }
    if (index === CONST.SQUARES_SUM * CONST.SQUARES_SUM - 1) {
      return { boxShadow: 'inset 0px 0px 0px 500px black' };
    }
  };

  const calculateInitialPosition = (position: PositionType, index: number): CSSProperties => {
    if (index > 0) {
      left += CONST.SQUARES_WIDTH / CONST.SQUARES_SUM;
    }
    if (index > 0 && index % CONST.SQUARES_SUM === 0) {
      top += CONST.SQUARES_WIDTH / CONST.SQUARES_SUM;
      left = 0;
    }

    return {
      backgroundPosition: `${position.x}px ${position.y}px`,
      top: top + '%',
      left: left + '%',
    };
  };

  return (
    <>
      {slicedImages.map((slicedImage: MappedImagesType, index: number) => (
        <>
          <ImageSlice
            key={index}
            style={createInlineCss(slicedImage.position, index, slicedImage.index)}
          />
        </>
      ))}
    </>
  );
};
