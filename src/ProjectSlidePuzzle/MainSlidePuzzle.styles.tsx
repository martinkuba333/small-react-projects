import styled from 'styled-components';
import { CONST } from './Utils/Constants';

export const Board = styled.div`
  background: green;
  width: ${CONST.SQUARES_SUM * CONST.SQUARES_WIDTH + 200}px;
  height: ${CONST.SQUARES_SUM * CONST.SQUARES_WIDTH + 250}px;
  margin: 60px auto;
  padding-top: 50px;
  position: relative;
`;

export const ImageContainer = styled.div`
  width: ${CONST.SQUARES_WIDTH * CONST.SQUARES_SUM}px;
  height: ${CONST.SQUARES_WIDTH * CONST.SQUARES_SUM}px;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  padding: 5px;
  pointer-events: none;
`;

export const ButtonContainer = styled.div`
  background: white;
  width: 80px;
  height: 47px;
  display: block;
  margin: 25px auto;
  position: relative;
  padding: 5px;
`;

export const Shuffle = styled.button`
  margin-left: 25%;
  display: inline-block;
  background: black;
  padding: 10px;
  color: white;
  border: 1px solid white;
  text-transform: uppercase;
  font-weight: bold;
  border-radius: 5px;

  &:hover {
    cursor: pointer;
  }
`;

export const Solution = styled.button`
  position: absolute;
  right: 25%;
  display: inline-block;
  background: black;
  padding: 10px;
  color: white;
  border: 1px solid white;
  text-transform: uppercase;
  font-weight: bold;
  border-radius: 5px;

  &:hover {
    cursor: pointer;
  }
`;

export const InfoText = styled.div`
  text-align: center;
  color: white;
  margin-top: 10px;
  font-size: 18px;
`;

export const OriginalImage = styled.div`
  background-image: url('/images/${CONST.IMAGE}');
  background-position: center center;
  width: 280px;
  height: 280px;
  background-repeat: no-repeat;
  margin: auto;
  text-align: center;
  position: absolute;
  left: 50%;
  top: 90px;
  transform: translateX(-50%);
  box-shadow: 0 0 20px 20px black;
`;
