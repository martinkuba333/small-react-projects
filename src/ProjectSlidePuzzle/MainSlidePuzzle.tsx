import React, { useEffect, useState } from 'react';
import {
  Board,
  ButtonContainer,
  ImageContainer,
  InfoText,
  OriginalImage,
  Shuffle,
  Solution,
} from './MainSlidePuzzle.styles';
import { Images } from './Images/Image';
import { Arrows } from './Arrows/Arrows';

export const MainSlidePuzzle: React.FC = (): JSX.Element => {
  const [up, setUp] = useState<boolean>(false);
  const [left, setLeft] = useState<boolean>(false);
  const [right, setRight] = useState<boolean>(false);
  const [down, setDown] = useState<boolean>(false);
  const [shuffle, setShuffle] = useState<boolean>(false);
  const [isOriginalImageVisible, setIsOriginalImageVisible] = useState<boolean>(false);

  const imageContainer = (): JSX.Element => {
    return (
      <ImageContainer>
        <Images
          up={up}
          setUp={setUp}
          left={left}
          setLeft={setLeft}
          right={right}
          setRight={setRight}
          down={down}
          setDown={setDown}
          shuffle={shuffle}
          setShuffle={setShuffle}
        />
      </ImageContainer>
    );
  };

  const arrowContainer = (): JSX.Element => {
    return (
      <ButtonContainer>
        <Arrows
          up={up}
          setUp={setUp}
          left={left}
          setLeft={setLeft}
          right={right}
          setRight={setRight}
          down={down}
          setDown={setDown}
        />
      </ButtonContainer>
    );
  };

  const shuffleImages = () => {
    setShuffle(true);
  };

  const createButtons = (): JSX.Element => {
    return (
      <>
        <Shuffle onClick={() => shuffleImages()}>Shuffle</Shuffle>
        <Solution
          onMouseEnter={() => setIsOriginalImageVisible(true)}
          onMouseLeave={() => setIsOriginalImageVisible(false)}
        >
          Solution
        </Solution>
      </>
    );
  };

  return (
    <>
      <Board>
        {imageContainer()}
        <InfoText>Move the black square and recreate the image.</InfoText>
        <InfoText>Hover on solution for solution.</InfoText>
        {arrowContainer()}
        {createButtons()}
        <OriginalImage style={{ display: isOriginalImageVisible ? 'block' : 'none' }} />
      </Board>
    </>
  );
};
