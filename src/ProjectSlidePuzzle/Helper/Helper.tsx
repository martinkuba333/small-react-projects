import { PositionType } from '../Images/Image';
import { CONST } from '../Utils/Constants';

export const createBgPositionPoints = (): PositionType[] => {
  const result = [];
  let x = 0;
  let y = CONST.SQUARES_WIDTH;
  let index = 0;
  for (let i = 0; i < CONST.SQUARES_SUM; i++) {
    for (let j = 0; j < CONST.SQUARES_SUM; j++) {
      x -= CONST.SQUARES_WIDTH;
      if (j === 0) {
        y -= CONST.SQUARES_WIDTH;
        x = 0;
      }
      result.push({ x, y, index });
      index++;
    }
  }

  return result;
};
