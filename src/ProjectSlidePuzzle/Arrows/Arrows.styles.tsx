import styled from 'styled-components';

const buttons = {
  position: 'absolute',
  width: '23px',
  textAlign: 'center',
};

export const ButtonUp = styled.button`
  ${buttons};
  left: 0;
  right: 0;
  margin: auto;
`;
export const ButtonLeft = styled.button`
  ${buttons};
  bottom: 5px;
  left: 5px;
`;
export const ButtonRight = styled.button`
  ${buttons};
  bottom: 5px;
  right: 5px;
`;
export const ButtonDown = styled.button`
  ${buttons};
  bottom: 5px;
  left: 0;
  right: 0;
  margin: auto;
`;
