import React, { useEffect } from 'react';
import { ButtonDown, ButtonLeft, ButtonRight, ButtonUp } from './Arrows.styles';

interface ArrowsInterface {
  up: boolean;
  left: boolean;
  right: boolean;
  down: boolean;
  setUp: (value: boolean) => void;
  setLeft: (value: boolean) => void;
  setRight: (value: boolean) => void;
  setDown: (value: boolean) => void;
}

export const Arrows: React.FC<ArrowsInterface> = (props: ArrowsInterface): JSX.Element => {
  useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent) => {
      switch (event.key) {
        case 'ArrowUp':
          buttonUpPressed();
          break;
        case 'ArrowLeft':
          buttonLeftPressed();
          break;
        case 'ArrowRight':
          buttonRightPressed();
          break;
        case 'ArrowDown':
          buttonDownPressed();
          break;
        default:
          break;
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  const createArrows = (): JSX.Element => {
    return (
      <>
        <ButtonUp key={'up'} onClick={buttonUpPressed}>
          &#8593;
        </ButtonUp>
        <ButtonLeft key={'left'} onClick={buttonLeftPressed}>
          &#8592;
        </ButtonLeft>
        <ButtonRight key={'right'} onClick={buttonRightPressed}>
          &#8594;
        </ButtonRight>
        <ButtonDown key={'down'} onClick={buttonDownPressed}>
          &#8595;
        </ButtonDown>
      </>
    );
  };

  const buttonUpPressed = () => {
    props.setUp(true);
  };

  const buttonLeftPressed = () => {
    props.setLeft(true);
  };

  const buttonRightPressed = () => {
    props.setRight(true);
  };

  const buttonDownPressed = () => {
    props.setDown(true);
  };

  return <>{createArrows()}</>;
};
