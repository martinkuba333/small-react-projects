import React, { CSSProperties, useEffect, useState } from 'react';
import { MovingCircle } from './Circle.styles';
import { CONST } from '../Utils/Const';

interface CircleInterface {
  tick: number;
  size: number;
  index: number;
  initialTop: number;
  initialLeft: number;
  initialAngle: number;
  bgColor: string;
}

export const Circle: React.FC<CircleInterface> = (props: CircleInterface) => {
  const [angle, setAngle] = useState<number>(props.initialAngle);
  const [top, setTop] = useState<number>(props.initialTop);
  const [left, setLeft] = useState<number>(props.initialLeft);

  useEffect(() => {
    const radius = props.size / 4;
    const angularSpeed = 0.004 * (CONST.NUM_OF_CIRCLES - props.index + 1);

    const newAngle = angle + angularSpeed;
    setAngle(newAngle);

    const newTop = props.size / 4 + radius * Math.sin(newAngle);
    const newLeft = props.size / 4 + radius * Math.cos(newAngle);

    setTop(newTop);
    setLeft(newLeft);
  }, [props.tick]);

  const calculatePosition = (): CSSProperties => {
    return {
      top: top + 'px',
      left: left + 'px',
    };
  };

  return (
    <>
      <MovingCircle
        style={calculatePosition()}
        size={props.size}
        index={props.index}
        bgColor={props.bgColor}
      />
    </>
  );
};
