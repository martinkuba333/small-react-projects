import styled from 'styled-components';

export interface PropsCircle {
  size: number;
  index: number;
  bgColor: string;
}

export const MovingCircle = styled.div<PropsCircle>`
  width: ${(props) => props.size / 2}px;
  height: ${(props) => props.size / 2}px;
  display: block;
  border-radius: ${(props) => props.size / 2}px;
  background: ${(props) => props.bgColor};
  position: absolute;
`;
