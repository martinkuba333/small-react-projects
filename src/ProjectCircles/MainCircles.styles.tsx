import styled from 'styled-components';
import { CONST } from './Utils/Const';
import { PropsCircle } from './Circle/Circle.styles';

export const Board = styled.div`
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  overflow: hidden;
`;

export const Container = styled.div`
  background: #3f5e98;
  margin-left: calc(50% - 100px);
  margin-top: 50px;
  position: relative;
`;

export const CircleContainer = styled.div<PropsCircle>`
  width: ${(props) => props.size}px;
  height: ${(props) => props.size}px;
  display: block;
  background: ${(props) => props.bgColor};
  border-radius: ${(props) => props.size}px;
  position: absolute;
  left: ${(props) => CONST.BASIC_SIZE * 2 - (props.index * CONST.BASIC_SIZE) / 2}px;
  top: ${(props) => CONST.BASIC_SIZE * 2 - (props.index * CONST.BASIC_SIZE) / 2}px;
  //border: 1px solid black;
`;
