import React, { useEffect, useState } from 'react';
import { Circle } from './Circle/Circle';
import { Board, CircleContainer, Container } from './MainCircles.styles';
import { CONST } from './Utils/Const';

type CircleType = {
  top: number;
  left: number;
  angle: number;
  bgColor: string;
};

export const MainCircles = (): JSX.Element => {
  const [tick, setTick] = useState<number>(0);
  let keyCounter = 0;
  const circleArray: CircleType[] = [
    { top: 20, left: 40, angle: 0.02, bgColor: 'RGBA(203, 191, 120, 0.1)' },
    { top: 22.955, left: 39.553, angle: 0.16, bgColor: 'RGBA(203, 191, 120, 0.2)' },
    { top: 25.91, left: 39.106, angle: 0.3, bgColor: 'RGBA(203, 191, 120, 0.3)' },
    { top: 27.055, left: 38.128, angle: 0.42, bgColor: 'RGBA(203, 191, 120, 0.4)' },
    { top: 30.2, left: 37.15, angle: 0.54, bgColor: 'RGBA(203, 191, 120, 0.5)' },
    { top: 32.807, left: 34.945, angle: 0.71, bgColor: 'RGBA(203, 191, 120, 0.6)' },
    { top: 35.414, left: 32.74, angle: 0.88, bgColor: 'RGBA(203, 191, 120, 0.7)' },
    { top: 37.702, left: 26.075, angle: 1.24, bgColor: 'RGBA(203, 191, 120, 0.8)' },
    { top: 39.99, left: 19.41, angle: 1.6, bgColor: 'RGBA(203, 191, 120, 1)' },
  ];

  useEffect(() => {
    const interval = setInterval(() => {
      setTick((prevTick) => prevTick + 1);
    }, 1);

    return () => clearInterval(interval);
  }, []);

  const createCircles = (): JSX.Element => {
    const result = [];

    for (let i = CONST.NUM_OF_CIRCLES; i > 0; i--) {
      let circles = fillCirclesWithValues(i);

      result.push(
        <CircleContainer size={CONST.BASIC_SIZE * i} index={i} bgColor={'#3f5e98'}>
          {circles}
        </CircleContainer>,
      );
    }
    return <>{result}</>;
  };

  const fillCirclesWithValues = (i: number): JSX.Element[] => {
    return circleArray.map((circle, j) => (
      <Circle
        key={j}
        tick={tick}
        size={CONST.BASIC_SIZE * i}
        index={i}
        initialTop={circle.top}
        initialLeft={circle.left}
        initialAngle={circle.angle}
        bgColor={circle.bgColor}
      />
    ));
  };

  return (
    <>
      <Board>
        <Container>{createCircles()}</Container>
      </Board>
    </>
  );
};
