import React, { useEffect, useState } from 'react';
import { Board } from './Board/Board';

export const MainWeaves: React.FC = (): JSX.Element => {
  return (
    <>
      <Board />
    </>
  );
};
