import React, { useEffect, useState } from 'react';
import { CONST } from '../Utils/Const';
import { Line } from '../Line/Line';
import { Area, LeftLinesContainer, RightLinesContainer } from './Board.styles';

const SUM_OF_LINES = 16;

export const Board: React.FC = (): JSX.Element => {
  const [tick, setTick] = useState<number>(0);

  useEffect(() => {
    const squareInterval = setTimeout(() => {
      setTick((prevTick: number) => prevTick + 1);
    }, CONST.LINE_INTERVAL);
    return () => clearTimeout(squareInterval);
  }, [tick]);

  const createLeftLines = (): JSX.Element[] => {
    return Array.from({ length: SUM_OF_LINES }, (_, index) => (
      <Line key={index} tick={tick} rotateLeft={true} index={index} />
    ));
  };

  const createRightLines = (): JSX.Element[] => {
    return Array.from({ length: SUM_OF_LINES }, (_, index) => (
      <Line key={index} tick={tick} rotateLeft={false} index={index} />
    ));
  };

  return (
    <Area>
      <LeftLinesContainer>{createLeftLines()}</LeftLinesContainer>
      <RightLinesContainer>{createRightLines()}</RightLinesContainer>
    </Area>
  );
};
