import styled from 'styled-components';

export const Area = styled.div`
  position: absolute;
  margin: auto;
  left: 0;
  right: 0;
  width: 700px;
  height: 700px;
  top: -90px;
  //background: #0f0f0f;
`;

export const LeftLinesContainer = styled.div`
  position: absolute;
  left: 0px;
  top: 50px;
  margin: auto;
  display: block;
`;

export const RightLinesContainer = styled.div`
  position: absolute;
  left: 200px;
  top: 50px;
  margin: auto;
  display: block;
`;
