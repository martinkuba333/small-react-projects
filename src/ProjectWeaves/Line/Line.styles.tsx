import styled from 'styled-components';
import { CONST } from '../Utils/Const';

export const RectangleContainer = styled.div`
  width: ${CONST.RECTANGLE_WIDTH}px;
  height: ${CONST.LINE_HEIGHT * 100}px;
  position: absolute;
  right: 0;
  //outline: 1px solid black;
`;

export const Rectangle = styled.div`
  width: ${CONST.RECTANGLE_WIDTH}px;
  height: ${CONST.LINE_HEIGHT}px;
  position: absolute;
  border: 0 solid transparent;
`;
