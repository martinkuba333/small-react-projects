import React, { CSSProperties, useEffect, useState } from 'react';
import { Rectangle, RectangleContainer } from './Line.styles';
import { CONST } from '../Utils/Const';

interface LineInterface {
  tick: number;
  rotateLeft: boolean;
  index: number;
}

type VisibleIntervalType = [number, number];

const sumOfRectangles = 100;

export const Line: React.FC<LineInterface> = (props: LineInterface): JSX.Element => {
  const dispersion = Math.floor(sumOfRectangles / 2 + sumOfRectangles / 4);
  const [visibleInterval, setVisibleInterval] = useState<VisibleIntervalType>([0, dispersion]);
  const [moveIncrement, setMoveIncrement] = useState<-1 | 1>(1);

  useEffect(() => {
    if (props.tick > props.index * 4) {
      setVisibleInterval((prevState: VisibleIntervalType): VisibleIntervalType => {
        let newStart = prevState[0] + moveIncrement;
        let newEnd = prevState[1] + moveIncrement;

        if (newStart <= 0) {
          setMoveIncrement(1);
        }
        if (newStart >= sumOfRectangles - dispersion - 1) {
          setMoveIncrement(-1);
        }

        return [newStart, newEnd];
      });
    }
  }, [props.tick]);

  const createRectangles = (): JSX.Element[] => {
    const squareSum = sumOfRectangles;
    let rectangles = [];
    for (let i = 0; i < squareSum; i++) {
      rectangles.push(<Rectangle key={i} style={createLineInlineCss(i)} />);
    }
    return rectangles;
  };

  const createLineInlineCss = (index: number): CSSProperties => {
    const style = {
      background: 'transparent',
      borderTopLeftRadius: '0px',
      borderTopRightRadius: '0px',
      borderBottomRightRadius: '0px',
      borderBottomLeftRadius: '0px',
      top: index * CONST.LINE_HEIGHT + 'px',
      opacity: 1,
    };
    if (index >= visibleInterval[0] && index <= visibleInterval[1]) {
      style.background = props.rotateLeft ? '#f00101' : 'pink';
    }
    if (index === visibleInterval[0]) {
      style.borderTopLeftRadius = '5px';
      style.borderTopRightRadius = '5px';
    }
    if (index === visibleInterval[1]) {
      style.borderBottomLeftRadius = '5px';
      style.borderBottomRightRadius = '5px';
    }
    // const evenNums = [24, 25, 31, 32, 38, 39, 45, 46, 52, 53, 59, 60, 66, 67, 73, 74];
    // const oddNums = [28, 29, 35, 36, 42, 43, 49, 50, 56, 57, 63, 64, 70, 71, 77, 78];
    // if (!props.rotateLeft) {
    //   if (props.index % 2 === 0) {
    //     if (evenNums.indexOf(index) > -1) {
    //       style.opacity = 0;
    //     }
    //   } else {
    //     if (oddNums.indexOf(index) > -1) {
    //       style.opacity = 0;
    //     }
    //   }
    // }
    return style;
  };

  const createRectContainerInlineCss = (): CSSProperties => {
    const style = {
      transform: 'rotate(45deg)',
      top: props.index * CONST.RECTANGLE_WIDTH + 'px',
      left: props.index * CONST.RECTANGLE_WIDTH + 'px',
      zIndex: props.index,
    };
    if (props.rotateLeft) {
      style.transform = 'rotate(-45deg)';
      style.left = 410 - CONST.RECTANGLE_WIDTH * props.index + 'px';
    }
    return style;
  };

  return (
    <>
      <RectangleContainer style={createRectContainerInlineCss()}>
        {createRectangles()}
      </RectangleContainer>
    </>
  );
};
